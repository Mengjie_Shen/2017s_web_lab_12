package ictgradschool.web.lab12.ex3Dao;

import ictgradschool.Keyboard;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.util.mysql.MySQLDatabase;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3Dao.generated.Tables.PFILMS_FILM;

/**
 * Created by mshe666 on 4/01/2018.
 */
public class Exercise03 {
    public static void main(String[] args) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {


            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get all info about all films
            Result<Record> allFilmRecords = create.select().from(PFILMS_FILM).fetch();

            try (FilmDao filmDao = new FilmDao(new MySQLDatabase(), conn)) {
                System.out.println("Welcome to the Film database!");

                String userInput = "";
                while (!userInput.equals("4")) {

                    while (!userInput.equals("") && !userInput.equals("4")) {
                        List<Film> films = new ArrayList<>();
                        String queryInput;
                        switch (userInput) {
                            case "1":
                                System.out.println("Please enter the name of the actor you wish to get information about," +
                                        " or press enter to return to the previous menu");
                                queryInput = Keyboard.readInput();
                                if (queryInput.equals("")) {
                                    userInput = "";
                                    break;}
                                films = filmDao.getFilmByActorName(queryInput);
                                if (films.size() == 0) {
                                    System.out.println("Sorry, we couldn't find any actor by that name.");
                                    break;
                                }
                                for (Film film : films) {
                                    System.out.println(film.getTitle() + " (" + film.getActors().get(queryInput) + ")");
                                }
                                break;
                            case "2":
                                System.out.println("Please enter the name of the film you wish to get information about," +
                                        " or press enter to return to the previous menu");
                                queryInput = Keyboard.readInput();
                                if (queryInput.equals("")) {
                                    userInput = "";
                                    break;}
                                films = filmDao.getFilmByTitle(queryInput);
                                if (films.size() == 0) {
                                    System.out.println("Sorry, we couldn't find any film by that name.");
                                    break;
                                }
                                for (Film film : films) {
                                    for (String key : film.getActors().keySet()) {
                                        String actor_name = key;
                                        String role_name = film.getActors().get(key);
                                        System.out.println(actor_name + " (" + role_name + ")");
                                    }
                                }
                                break;
                            case "3":
                                System.out.println("Please enter the name of the genre you wish to get information about," +
                                        " or press enter to return to the previous menu");
                                queryInput = Keyboard.readInput();
                                if (queryInput.equals("")) {
                                    userInput = "";
                                    break;}
                                films = filmDao.getFilmByGenre(queryInput);
                                if (films.size() == 0) {
                                    System.out.println("Sorry, we couldn't find any film by that genre.");
                                    break;
                                }
                                for (Film film : films) {
                                    System.out.println(film.getTitle());
                                }
                                break;
                            case "4":
                                break;

                        }

                    }

                    System.out.println("Please select an option from the following:");
                    System.out.println("1. Information by Actor");
                    System.out.println("2. Information by Movie");
                    System.out.println("3. Information by Genre");
                    System.out.println("4. Exit");

                    userInput = Keyboard.readInput();
                }

                System.out.println("Bye!!");
            }



        }

    }
}
