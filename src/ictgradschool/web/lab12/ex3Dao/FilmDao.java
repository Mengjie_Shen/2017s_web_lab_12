package ictgradschool.web.lab12.ex3Dao;


import org.jooq.util.Database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mshe666 on 10/01/2018.
 */
public class FilmDao implements AutoCloseable {
    private final Database db;
    private final Connection conn;

    public FilmDao(Database db, Connection conn) throws IOException, SQLException {
        this.db = db;
//        this.conn = db.getConnection();
        this.conn = conn;
    }

    public List<Film> getFilmByTitle(String s) throws SQLException{
        List<Film> films = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT film_id, film_title FROM pfilms_film WHERE film_title = ? ;")) {
            stmt.setString(1, s);
            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {
                    int film_id = rs.getInt(1);
                    String film_title = rs.getString(2);

                    films.add(new Film(film_id, film_title));
                }
            }
        }

        for (Film film : films) {
            int film_id = film.getId();

            try (PreparedStatement stmt = conn.prepareStatement("SELECT pfilms_participates_in.actor_id, film_id, " +
                    "pfilms_participates_in.role_id, " +
                    "pfilms_actor.actor_fname, pfilms_actor.actor_lname, pfilms_role.role_name " +
                    "FROM pfilms_participates_in " +
                    "JOIN pfilms_actor ON pfilms_participates_in.actor_id = pfilms_actor.actor_id " +
                    "JOIN pfilms_role ON pfilms_participates_in.role_id = pfilms_role.role_id " +
                    "WHERE film_id = ? ;")) {
                stmt.setInt(1, film_id);

                try (ResultSet rs = stmt.executeQuery()) {

                    while (rs.next()) {
                        String name = rs.getString(4) + " " + rs.getString(5);
                        String role = rs.getString(6);
                        film.addActor(name, role);
                    }
                }
            }
        }

        return films;
    }

    public List<Film> getFilmByActorName(String s) throws SQLException{
        List<Film> films = new ArrayList<>();
        Map<Integer, String> actor_ids = new HashMap<>();
        String fname = null;
        String lname = null;
        if (s.split(" ").length > 1) {
            fname = s.split(" ")[0];
            lname = s.substring(s.indexOf(" ") + 1);
        }else {
            fname = s;
        }

        if (lname == null) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT actor_id FROM pfilms_actor WHERE actor_fname = ? ;")) {
                stmt.setString(1, fname);
                try (ResultSet rs = stmt.executeQuery()) {

                    while (rs.next()) {
                        int actor_id = rs.getInt(1);

                        actor_ids.put(actor_id, fname);
                    }
                }
            }
        }else {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT actor_id FROM pfilms_actor WHERE actor_fname = ? AND actor_lname = ? ;")) {
                stmt.setString(1, fname);
                stmt.setString(2, lname);
                try (ResultSet rs = stmt.executeQuery()) {

                    while (rs.next()) {
                        int actor_id = rs.getInt(1);

                        actor_ids.put(actor_id, fname + " " + lname);
                    }
                }
            }
        }

        for (int actor_id : actor_ids.keySet()) {
            try (PreparedStatement stmt = conn.prepareStatement("SELECT actor_id, pfilms_participates_in.film_id, " +
                    "pfilms_participates_in.role_id, " +
                    "pfilms_film.film_title, pfilms_role.role_name " +
                    "FROM pfilms_participates_in " +
                    "JOIN pfilms_film ON pfilms_participates_in.film_id = pfilms_film.film_id " +
                    "JOIN pfilms_role ON pfilms_participates_in.role_id = pfilms_role.role_id " +
                    "WHERE actor_id = ? ;")) {
                stmt.setInt(1, actor_id);
                try (ResultSet rs = stmt.executeQuery()) {

                    while (rs.next()) {
                        int film_id = rs.getInt(2);
                        int role_id = rs.getInt(3);
                        String film_title = rs.getString(4);
                        String role_name = rs.getString(5);

                        Film film = new Film(film_id, film_title);
                        film.addActor(actor_ids.get(actor_id), role_name);
                        films.add(film);
                    }
                }
            }
        }

        return films;
    }

    public List<Film> getFilmByGenre(String s) throws SQLException {
        List<Film> films = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT film_id, film_title FROM pfilms_film WHERE genre_name = ? ;")) {
            stmt.setString(1, s);
            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {
                    int film_id = rs.getInt(1);
                    String film_title = rs.getString(2);
                    Film film = new Film(film_id, film_title);
                    films.add(film);
                }
            }
        }

        return films;
    }


    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
